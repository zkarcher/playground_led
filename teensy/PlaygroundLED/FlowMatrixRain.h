#ifndef FLOW_MATRIX_RAIN_H__
#define FLOW_MATRIX_RAIN_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowMatrixRain : public Flow_Base {
public:
	FlowMatrixRain() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual float bestTransitionTime();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowMatrixRain::title() {
	return "FlowMatrixRain";
}

float FlowMatrixRain::speedMult() {
	return randRange(3.0f, 4.5f);
}

float FlowMatrixRain::bestTransitionTime() {
	return randRange(3.0f,3.5f);
}

void FlowMatrixRain::reset( float _map[LED_COUNT] ) {
	float offset;
	float inc;

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		offset = randRange(-0.5f,0.5f);
		inc = randRange(0.005f,0.1f);

		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			_map[MAPPOS(x,y)] = offset + (y-CENTER_Y)*inc;
		}
	}
}

#endif
