#ifndef FLOW_CENTRIFUGE_H__
#define FLOW_CENTRIFUGE_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowCentrifuge : public Flow_Base {
public:
	FlowCentrifuge() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowCentrifuge::title() {
	return "FlowCentrifuge";
}

float FlowCentrifuge::speedMult() {
	return randRange(1.0f, 2.0f);
}

void FlowCentrifuge::reset( float _map[LED_COUNT] ) {
	bool dir = randf() < 0.5f;

	float noise = randTo(0.05f);

	float skew = randRange(0.75f,1.3f);
	float invSkew = 0.5f / pow(0.5f, skew);

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			bool pivotLeft = (x < CENTER_X);
			if( x==CENTER_X ) {
				pivotLeft = (y & 0x1);
			}

			float cx = CENTER_X * (pivotLeft ? 0.5f : 1.5f);

			float angle;
			if( pivotLeft ) {
				angle = atan2f( y-CENTER_Y, x-cx );
			} else {
				angle = atan2f( y-CENTER_Y, -(x-cx) );
			}
			angle *= (1.0f / (M_PI*2.0f));	// Range -0.5..0.5 I think

			bool isPositive = angle > 0.0f;
			angle = powf( abs(angle), skew );
			angle *= (isPositive ? invSkew : -invSkew);

			_map[MAPPOS(x,y)] = angle * (dir ? 1.0f : -1.0f) + randTo(noise);
		}
	}
}

#endif
