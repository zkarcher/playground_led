#ifndef FRAME_PARAMS_H__
#define FRAME_PARAMS_H__

struct FrameParams {
	// animations are cooked at 60fps. 1.0==exactly 60fps.
	// 4.0==achieving 15fps, so move 4x faster.
  float timeMult;
  float brightness;
};

#endif
