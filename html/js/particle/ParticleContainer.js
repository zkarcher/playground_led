function ParticleContainer() {
	var self = this;

	self.spr = new PIXI.Container();

	self.grid = [];
	self.topWidth = 10 * 0;
	self.bottomWidth = self.topWidth;

	self.particles = [];
	self.timeSinceNewParticle = Infinity;

	for( var i=0; i<PARTICLE_WIDTH; i++ ) {
		var column = [];
		self.grid.push(column);

		for( var j=0; j<PARTICLE_HEIGHT; j++ ) {
			var spr = new PIXI.Sprite.fromImage('p/square.png');
			spr.width = spr.height = PARTICLE_SIZE;
			spr.x = i*PARTICLE_SPACING;
			spr.y = j*PARTICLE_SPACING;
			spr.alpha = rand();

			column.push(spr);
			self.spr.addChild(spr);
		}
	}

	self.getBrightnessAtCoord = function(coord){
		var floorX = Math.floor(coord.x);
		var ceilX = floorX + 1;
		var floorX_alpha = 1.0 - (coord.x - floorX);
		var ceilX_alpha = 1.0 - floorX_alpha;

		var floorY = Math.floor(coord.y);
		var ceilY = floorY + 1;
		var floorY_alpha = 1.0 - (coord.y - floorY);
		var ceilY_alpha = 1.0 - floorY_alpha;

		function getBrightness( xInt, yInt ){
			if( xInt < 0 ) return 0;
			if( yInt < 0 ) return 0;
			if( xInt >= self.grid.length ) return 0;
			if( yInt >= self.grid[0].length ) return 0;

			return self.grid[xInt][yInt].alpha;
		}

		return (
			(getBrightness( floorX, floorY ) * floorX_alpha * floorY_alpha) +
			(getBrightness( ceilX, floorY ) * ceilX_alpha * floorY_alpha) +
			(getBrightness( floorX, ceilY ) * floorX_alpha * ceilY_alpha) +
			(getBrightness( ceilX, ceilY ) * ceilX_alpha * ceilY_alpha)
		);
	}

	self.perFrame = function(timeMult){
		// Clear (or decay?) the grid
		const DECAY_RATE = 0.015;

		_.each( self.grid, function(column){
			_.each( column, function(spr){
				spr.alpha = Math.max( spr.alpha - DECAY_RATE*timeMult, 0.0 );
			})
		})

		self.timeSinceNewParticle += timeMult / 60.0;
		var rando = rand(1.0 / self.timeSinceNewParticle) < 0.005;
		if( self.timeSinceNewParticle < 1.0 ) rando = false;

		if( rando || (self.particles.length===0) ) {
			var p = new Particle( self.grid );
			self.particles.push( p );

			self.timeSinceNewParticle = 0.001;
		}

		for( var i=self.particles.length-1; i>=0; i-- ) {
			var p = self.particles[i];
			p.perFrame( timeMult, self.grid, self.topWidth, self.bottomWidth );

			// Splice out dead particles
			if( p.y > PARTICLE_HEIGHT ) {
				self.particles.splice(i,1);
			}
		}
	}
}
