function SemiCircleAnim() {
	var self = this;

	self.map = [];
	for( var i=0; i<LED_ACROSS; i++ ) {
		var column = [];
		self.map.push( column );

		for( var j=0; j<LED_DOWN; j++ ) {
			column[j] = new PIXI.Point(0,0);
		}
	}

	self.warblePhase = 0.0;

	self.perFrame = function( timeMult, particleContainer, leds ){

		self.warblePhase += timeMult * 0.05;

		for( var i=0; i<LED_ACROSS; i++ ) {
			var column = self.map[i];

			for( var j=0; j<LED_DOWN; j++ ) {
				var diff = new PIXI.Point( i-(LED_ACROSS-1)/2, j );
				var distance = Math.sqrt( diff.x*diff.x + diff.y*diff.y );

				const LEAVES = 7;
				var angle = Math.atan2( diff.y, diff.x );
				if( (LEAVES%2) === 0 ) {
					angle += Math.PI/(LEAVES*2);
				}

				angle += Math.sin( distance/3 + self.warblePhase ) * 0.1;

				var wavy = (Math.sin( angle * LEAVES ) + 1) * 0.5;	// 0..1

				column[j] = new PIXI.Point( 0, distance / lerp(1.5,3.0,wavy) );
			}
		}

	}
}
