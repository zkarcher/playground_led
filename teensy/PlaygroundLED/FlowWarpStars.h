#ifndef FLOW_WARP_STARS_H__
#define FLOW_WARP_STARS_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowWarpStars : public Flow_Base {
public:
	FlowWarpStars() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual float bestTransitionTime();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowWarpStars::title() {
	return "FlowWarpStars";
}

float FlowWarpStars::speedMult() {
	return randRange(1.5f, 2.2f);
}

float FlowWarpStars::bestTransitionTime() {
	return randRange(2.5f,4.0f);
}

void FlowWarpStars::reset( float _map[LED_COUNT] ) {
	// Split the scene into ~24 radial bands. Each one should have a
	// deterministic offset & inc.
	float rayCount = randIntRange( 12, 25 );
	float randomRot = randf();

	float speed = randRange(1.2f, 2.0f);

	float centerValue = randRange(-0.25f,0.25f);

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			float angle = atan2f( y-CENTER_Y, x-CENTER_X ) + randomRot;
			float ray = floorf( (angle / (M_PI*2.0f)) * rayCount );

			float diffX = x - CENTER_X;
			float diffY = y - CENTER_Y;
			float distance = sqrtf( diffX*diffX + diffY*diffY );

			float offset = ray * 0.57;
			offset -= floorf(offset);
			offset -= 0.5f;

			float inc = 1.0f / max(0.1, distance);
			inc *= inc;
			inc *= speed;

			float value = offset - inc;

			// Stars at the center should skew towards centerValue
			float centerAmt = constrain( distance * 0.333f, 0, 1 );
			centerAmt *= centerAmt * centerAmt;
			//centerAmt *= centerAmt;
			value = lerp( centerValue + distance*0.1f, value, centerAmt );

			_map[MAPPOS(x,y)] = value;
		}
	}
}

#endif
