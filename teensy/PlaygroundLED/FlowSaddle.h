#ifndef FLOW_SADDLE_H__
#define FLOW_SADDLE_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowSaddle : public Flow_Base {
public:
	FlowSaddle() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowSaddle::title() {
	return "FlowSaddle";
}

float FlowSaddle::speedMult() {
	return randRange(1.0f, 2.0f);
}

void FlowSaddle::reset( float _map[LED_COUNT] ) {
	float spread = randRange(0.1f,0.4f);

	float noise = randTo(0.01f);

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			float distX = (float)(x - CENTER_X);
			float distY = (float)(y - CENTER_Y);

			float saddle = 1.0f / max( abs(distX*distY), 0.5f );
			saddle *= spread;
			saddle += (abs(distX) + abs(distY)) * -0.01f;

			_map[MAPPOS(x,y)] = 0.25f - saddle + randTo(noise);
		}
	}
}

#endif
