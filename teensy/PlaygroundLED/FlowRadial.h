#ifndef FLOW_RADIAL_H__
#define FLOW_RADIAL_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowRadial : public Flow_Base {
public:
	FlowRadial() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowRadial::title() {
	return "FlowRadial";
}

float FlowRadial::speedMult() {
	return randRange(1.0f, 2.0f);
}

void FlowRadial::reset( float _map[LED_COUNT] ) {
	float speed = 1.0f / randRange(18.0f, 40.0f);

	bool doRuffles = (randf() < 0.5f);

	float ruffleAmt = (doRuffles ? 1.0f : 0.0f) * speed * randRange(1.0f,1.8f);
	float ruffleCount = (randf()<0.333f) ? 3 : 7;

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			float distX = (float)(x - CENTER_X);
			float distY = (float)(y);
			float distance = sqrtf( distX*distX + distY*distY );

			float angle = atan2f( distY, distX ) * ruffleCount;
			float ruffle = cosf( angle - (float)(M_PI*0.5f) ) * ruffleAmt;

			_map[MAPPOS(x,y)] = distance * speed + randTo(0.01f) + ruffle;
		}
	}
}

#endif
