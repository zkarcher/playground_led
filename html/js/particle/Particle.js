function Particle( inGrid ){
	var self = this;

	self.fromX = randInt(inGrid.length) / (inGrid.length-1);	// 0..1
	self.toX = self.fromX;//rand();	// 0..1

	self.y = -1;
	self.dy = randRange(0.04,0.05);

	self.perFrame = function(timeMult, grid, topWidth, bottomWidth){
		self.y += self.dy * timeMult;

		var gridHeight = grid[0].length;
		var progressY = self.y / gridHeight;
		var x = lerp( self.fromX*topWidth, self.toX*bottomWidth, progressY );

		// Light up the grid
		function illuminate( xInt, yInt, alpha ) {
			if( (0 <= xInt) && (xInt < grid.length) ) {
				var column = grid[xInt];

				if( (0 <= yInt) && (yInt < column.length) ) {
					// Let's weight alpha brighter, so movement appears more consistent to human eyes
					alpha = Math.sin( alpha * Math.PI*0.5 );

					//column[yInt].alpha = Math.min( 1.0, column[yInt].alpha + alpha );
					column[yInt].alpha = Math.max( column[yInt].alpha, alpha );
				}
			}
		}

		var floorX = Math.floor(x);
		var ceilX = floorX + 1;
		var floorX_alpha = 1.0 - (x - floorX);
		var ceilX_alpha = 1.0 - floorX_alpha;

		var floorY = Math.floor(self.y);
		var ceilY = floorY + 1;
		var floorY_alpha = 1.0 - (self.y - floorY);
		var ceilY_alpha = 1.0 - floorY_alpha;

		illuminate( floorX, floorY, floorX_alpha * floorY_alpha );
		illuminate( ceilX, floorY, ceilX_alpha * floorY_alpha );
		illuminate( floorX, ceilY, floorX_alpha * ceilY_alpha );
		illuminate( ceilX, ceilY, ceilX_alpha * ceilY_alpha );
	}
}
