#ifndef FLOW_HORIZ_STRIPES_H__
#define FLOW_HORIZ_STRIPES_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowHorizStripes : public Flow_Base {
public:
	FlowHorizStripes() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual float bestTransitionTime();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowHorizStripes::title() {
	return "FlowHorizStripes";
}

float FlowHorizStripes::speedMult() {
	return randRange(2.0f, 3.0f);
}

float FlowHorizStripes::bestTransitionTime() {
	return randRange(3.0f,4.0f);
}

void FlowHorizStripes::reset( float _map[LED_COUNT] ) {
	float offset;
	float inc;

	for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
		// Sometimes, don't change
		if( (y==0) || (randf()>0.2f) ) {
			offset = randRange(-0.5f,0.5f);
			inc = randRange(0.005f,0.1f);
			if( randf() < 0.5f ) inc *= -1.0f;
		}

		for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
			_map[MAPPOS(x,y)] = offset + (x-CENTER_X)*inc;
		}
	}
}

#endif
