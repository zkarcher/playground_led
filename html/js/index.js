var renderer, stage, leds;
var particleContainer;
var anim;
var stats;

function init(){
	//Create the renderer
	renderer = PIXI.autoDetectRenderer(
		512, 512,
		{antialias: false, transparent: false, resolution: window.devicePixelRatio}
	);

	//Add the canvas to the HTML document
	document.body.appendChild(renderer.view);

	//Create a container object called the `stage`
	stage = new PIXI.Container();

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	document.body.appendChild( stats.domElement );

	leds = [];
	for (var i=0; i<LED_ACROSS; i++ ) {
		var column = [];
		leds.push(column);

		for (var j=0; j<LED_DOWN; j++ ) {
			var spr = new PIXI.Sprite.fromImage('p/glow_hard.png');
			spr.width = spr.height = LED_SIZE;
			spr.tint = randInt(0xffffff);
			spr.x = (i+1) * LED_SPACING;
			spr.y = (j+2) * LED_SPACING;
			spr.pivot.x = spr.pivot.y = LED_SIZE/2;
			spr.blendMode = PIXI.BLEND_MODES.SCREEN;

			column.push(spr);
			stage.addChild(spr);
		}
	}

	particleContainer = new ParticleContainer();
	stage.addChild( particleContainer.spr );
	particleContainer.spr.x = LED_SPACING;
	particleContainer.spr.y = (LED_DOWN+5) * LED_SPACING;

	anim = new SemiCircleAnim();

	render();
}

var blueAmt = 0.0;
var blueInc = 0.01;

var prevMS = _.now();	// perFrame timing
function render() {
	requestAnimationFrame( render );

	// Frame rate may not be constant 60fps. Time between frames determines how
	// quickly to advance animations.
	var ms = _.now();
	var elapsed = (ms - prevMS) * 0.001;	// ms to seconds
	var timeMult = elapsed * 60.0;			// Animations were all cooked at 60fps :P
	timeMult = Math.min( 4.0, timeMult );	// Prevent grievous skipping
	prevMS = ms;

	if(particleContainer) {
		particleContainer.perFrame(timeMult);
	}

	blueAmt += blueInc * timeMult;
	if( blueAmt >= 1.0 ) {
		blueInc = -Math.abs(blueInc);
	} else if( blueAmt <= 0 ) {
		blueInc = Math.abs(blueInc);
	}
	blueAmt = clamp(blueAmt,0,1);

	if(anim){
		anim.perFrame(timeMult, particleContainer, leds);

		// Using anim.map, grab brightness values from particleContainer.grid,
		// and set the LED colors.
		for( var i=0; i<LED_ACROSS; i++ ) {
			for( var j=0; j<LED_DOWN; j++ ) {
				var coord = anim.map[i][j];
				var brightness = particleContainer.getBrightnessAtCoord( coord );

				// Kinda magenta
				var color = Math.round(brightness*0xff);
				color = (color<<16) + Math.round(color * blueAmt);

				var ledSpr = leds[i][j];
				ledSpr.tint = color;
			}
		}
	}

	//Tell the `renderer` to `render` the `stage`
	renderer.render(stage);

	if( stats ) stats.update();
}

window.addEventListener("DOMContentLoaded", function() {
  init();
}, false);
