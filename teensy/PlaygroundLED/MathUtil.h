#ifndef MATH_UTIL_H__
#define MATH_UTIL_H__

#define randf()           ( (float)(random(0xffffff)) * (1.0f/(float)(0xffffff)) )
#define randTo(r)         ( randf() * (r) )
#define randRange(a,b)    ( (a) + randTo((b)-(a)) )
#define randInt(r)        ( random((r)) )
#define randIntRange(a,b) ( (a) + random((b)-(a)) )

#define lerp(a,b,p)       ( (a) + ((b)-(a)) * (p) )

#endif
