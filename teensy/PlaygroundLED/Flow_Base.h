#ifndef FLOW_BASE_H__
#define FLOW_BASE_H__

#include <Arduino.h>
#include "Const.h"
#include "FrameParams.h"

class Flow_Base {
public:
	Flow_Base(){};

	virtual String title();
	virtual float speedMult();
	virtual float bestTransitionTime();
	virtual void reset( float _map[LED_COUNT] );
	virtual void perFrame( FrameParams frameParams, float _map[LED_COUNT] );
};

String Flow_Base::title() {
	return "Flow_Base";
}

float Flow_Base::speedMult() {
	return 1.0f;
}

float Flow_Base::bestTransitionTime() {
	return randRange(5.0f, 7.0f);
}

void Flow_Base::reset( float _map[LED_COUNT] ) {
	// Extend me
}

void Flow_Base::perFrame( FrameParams frameParams, float _map[LED_COUNT] ) {
	// Extend me.
}

#endif
