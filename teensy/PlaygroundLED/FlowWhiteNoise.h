#ifndef FLOW_WHITE_NOISE_H__
#define FLOW_WHITE_NOISE_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowWhiteNoise : public Flow_Base {
public:
	FlowWhiteNoise() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual float bestTransitionTime();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowWhiteNoise::title() {
	return "FlowWhiteNoise";
}

float FlowWhiteNoise::speedMult() {
	return randRange(2.0f, 4.0f);
}

float FlowWhiteNoise::bestTransitionTime() {
	return randRange(2.0f,2.5f);
}

void FlowWhiteNoise::reset( float _map[LED_COUNT] ) {
	float radius = randRange(0.42f,0.5f);

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			_map[MAPPOS(x,y)] = randRange(-radius,radius);
		}
	}
}

#endif
