#ifndef FLOW_SPIRAL_H__
#define FLOW_SPIRAL_H__

#include <Arduino.h>
#include "Const.h"
#include "MathUtil.h"
#include "Flow_Base.h"

class FlowSpiral : public Flow_Base {
public:
	FlowSpiral() : Flow_Base() {};

	virtual String title();
	virtual float speedMult();
	virtual void reset( float _map[LED_COUNT] );
};

String FlowSpiral::title() {
	return "FlowSpiral";
}

float FlowSpiral::speedMult() {
	return randRange(2.0f, 3.0f);
}

void FlowSpiral::reset( float _map[LED_COUNT] ) {
	float speed = 1.0f / randRange(18.0f, 40.0f);

	float spiralAmt = speed * 1.2f;
	float arms = randRange(5.0f,13.0f);
	if( randf()<0.5f ) arms *= -1.0f;	// CW or CCW

	float pivotY = (randf() < 0.5f) ? 0 : (GRID_HEIGHT-0.5f);

	for( int_fast8_t x=0; x<GRID_WIDTH; x++ ) {
		for( int_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
			float distX = (float)(x - CENTER_X);
			float distY = (float)(y - pivotY);
			float distance = sqrtf( distX*distX + distY*distY );

			float angle = atan2f( distY, distX ) * arms;

			_map[MAPPOS(x,y)] = distance * speed + randTo(0.01f) + spiralAmt*angle;
		}
	}
}

#endif
