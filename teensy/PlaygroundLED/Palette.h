#ifndef PALETTE_H__
#define PALETTE_H__

#include <Arduino.h>
#include "Const.h"
#include "Color.h"
#include "MathUtil.h"
#include "FrameParams.h"

#define STOP_COUNT  (16)

#define COLOR_TWEEN_TIME  (4.5f)

// PaletteStop
struct Stop {
	// Color channel range: 0..0xff please
	float src_r;
	float src_g;
	float src_b;
	float _r;
	float _g;
	float _b;

	// Tweening
	float dest_r;
	float dest_g;
	float dest_b;
	float tweenRatio;

	float center;
	float radius;
	float freq;
	float position;
};

class Palette {
public:
	Palette(){};

	void init();
	void setStopDestColor( uint_fast8_t stopIdx, uint_fast8_t r, uint_fast8_t g, uint_fast8_t b );
	void chooseNewColors();
	void perFrame( FrameParams frameParams );
	RGB8 rgbAtPosition( float pos );

private:
	float phase = 0;
	Stop stops[STOP_COUNT];

	bool isTweening = false;
	float timeUntilTweenStart = 5.0f;
	float tweenTime = 0.0f;
};

void Palette::init() {
	for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
		uint_fast8_t r = (s<=1 || s==5) ? 0xff : 0;
		uint_fast8_t g = ((1<=s && s<=3)) ? 0xff : 0;
		uint_fast8_t b = ((3<=s && s<=5)) ? 0xff : 0;

		stops[s].src_r = pgm_read_byte(&LED_GAMMA[r]);
		stops[s].src_g = pgm_read_byte(&LED_GAMMA[g]);
		stops[s].src_b = pgm_read_byte(&LED_GAMMA[b]);

		stops[s].center = (float)(s*2+1) / (STOP_COUNT*2);
		stops[s].radius = 1.0f / (STOP_COUNT*4.1);
		stops[s].freq = randf() * 0.1f;
	}
}

// r,g,b: Range 0..0xff please
void Palette::setStopDestColor( uint_fast8_t stopIdx, uint_fast8_t r, uint_fast8_t g, uint_fast8_t b ) {
	uint_fast8_t _r = pgm_read_byte(&LED_GAMMA[r]);
	uint_fast8_t _g = pgm_read_byte(&LED_GAMMA[g]);
	uint_fast8_t _b = pgm_read_byte(&LED_GAMMA[b]);

	// No black pixels
	// FIXME: Doesn't seem to be working?
	/*
	if( (_r==0) && (_g==0) && (_b==0) ) {
		uint_fast8_t mx = max( max( r, g ), b );
		if( mx==r ) _r = 0x1;
		if( mx==g ) _g = 0x1;
		if( mx==b ) _b = 0x1;
	}
	*/

	stops[stopIdx].dest_r = _r;
	stops[stopIdx].dest_g = _g;
	stops[stopIdx].dest_b = _b;
}

void Palette::chooseNewColors() {
	// Possibilities:
	//   - Small chance of choosing a rainbow
	//   - Select a bright rave color & create a high-contrast palette with it
	//   - Select 2 rave colors

	// Rainbow
	// Fill 75% with color, final 25% is black
	if( randf() < 0.12f ) {
		float h = randInt(6) / 6.0f;
		float hueDir = (randf()<0.5f) ? -1.0f : 1.0f;

		for( uint_fast8_t s=0; s<STOP_COUNT; s+=2 ) {
			if( s < (STOP_COUNT*0.75) ) {
				RGB rgb = hsv2rgb(
					h + randRange(-0.03f,0.03f),
					// Desaturate the first color. :P I like how this particular bug looks.
					constrain( (s/2) + 0.2f + randRange(-0.05f,0.05f), 0.0f, 1.0f ),
					randRange(0.8f,1.0f)
				);
				this->setStopDestColor( s, rgb.r*0xff, rgb.g*0xff, rgb.b*0xff );
				this->setStopDestColor( s+1, rgb.r*0xff, rgb.g*0xff, rgb.b*0xff );

			} else {
				this->setStopDestColor( s, 0, 0, 0 );
				this->setStopDestColor( s+1, 0, 0, 0 );
			}

			h += (hueDir / 6.0f);
		}

	} else {
		//                           0red    1orange 2yellow 3cyan   4blue   5magenta
		const float FAVE_HUES[] =   {0.0f/6, 0.5f/6, 1.0f/6, 3.0f/6, 4.0f/6, 5.0f/6};

		const uint_fast8_t HUE_OPTS[] = {0,0,0,0,0, 1, 2,2, 3, 4,4,4,4,4, 5,5,5};
		uint_fast8_t h_i = HUE_OPTS[ randInt( sizeof(HUE_OPTS) / sizeof(uint_fast8_t) ) ];

		float baseHue0 = FAVE_HUES[h_i];

		// TODO: choose complimentary hues

		// Spice the palette a bit
		float hueRadius = 0.05f;//randRange(0.02f, 0.07f);
		float minSat = 0.8f;//randRange(0.8f, 0.95f);
		float minVal = 0.6f;//randRange(0.4f, 0.8f);

		// Build a palette with contrasting colors (light & dark)
		bool isLight = true;
		int_fast8_t stopsUntilSwitch = 1 + randInt(3);

		for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
			// When flip from light->dark: Sometimes put a white color in the palette
			bool whiteEdge = (stopsUntilSwitch==0) && (randf()<0.2f);

			// Set the color
			if( isLight ) {
				RGB rgb = hsv2rgb(
					baseHue0 + randRange(-hueRadius, hueRadius),
					randRange( minSat, 1.0f ) * (whiteEdge ? 0 : 1.0f),
					randRange( minVal, 1.0f )
				);
				this->setStopDestColor( s, rgb.r*0xff, rgb.g*0xff, rgb.b*0xff );

			} else {
				this->setStopDestColor( s, 0, 0, 0 );
			}

			// For the next stop: Switch light/dark?
			stopsUntilSwitch--;
			if( stopsUntilSwitch < 0 ) {
				stopsUntilSwitch = 1 + randInt(3);
				isLight = !isLight;
			}
		}
	}

	/*
	// Choose a new palette
	float p = randf();

	if( p < 0.1f ) {
		// Rainbow
		for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
			float r = randf();
			float g = randf();
			float b = randf();
			float mn = min( min( r, g ), b );
			if( mn==r ) r *= r*r;
			if( mn==g ) g *= g*g;
			if( mn==b ) b *= b*b;

			this->setStopDestColor( s, r*0xff, g*0xff, b*0xff );
		}

	} else if( p > 0.7f ) {
		// Blue & red
		float r=0, g=0, b=0;
		if( p > 0.85f ) {
			b = 0xff;
		} else {
			r = 0xff;
		}

		float brt = 1.0f;
		for( int_fast8_t s=STOP_COUNT-1; s>=0; s-- ) {
			float bdark = brt;
			if( s < (STOP_COUNT-1) ) bdark *= randf();
			this->setStopDestColor( s, r*bdark, g*bdark, b*bdark );

			// Put shocks of white in there
			if( randf() < 0.02f ) {
				this->setStopDestColor( s, 0xff, 0xff, 0xff );
			}

			// Taper darker
			brt *= randRange(0.45f, 0.95f);

			// Random wave crest
			if( randf() < 0.1f ) {
				brt = 1.0f;
			}
		}

	} else {
		// Variations on one or two hues
		float r0 = randTo(0xff);
		float g0 = randTo(0xff);
		float b0 = randTo(0xff);

		// Ensure the top component is bright
		float mx = max( max( r0, g0 ), b0 );
		if( mx==r0 ) r0 = max( r0, randRange(0x7f, 0xff) );
		if( mx==g0 ) g0 = max( g0, randRange(0x7f, 0xff) );
		if( mx==b0 ) b0 = max( b0, randRange(0x7f, 0xff) );

		bool doTwoHues = randf()<0.333f;
		float r1 = doTwoHues ? randTo(0xff) : r0;
		float g1 = doTwoHues ? randTo(0xff) : g0;
		float b1 = doTwoHues ? randTo(0xff) : b0;

		for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
			float brt = randf();

			// Moar darks plz
			if( brt > 0.5f ) {
				brt *= 2.0f;
				brt *= brt;
				brt *= 0.5f;
			}

			// Choose one of the two hues
			if( randf() < 0.5f ) {
				this->setStopDestColor( s, r0*brt, g0*brt, b0*brt );
			} else {
				this->setStopDestColor( s, r1*brt, g1*brt, b1*brt );
			}
		}
	}
	*/
}

void Palette::perFrame( FrameParams frameParams ) {
	float timeMult = frameParams.timeMult;
	float brightness = frameParams.brightness;

	if( !isTweening ) {
		timeUntilTweenStart -= timeMult * (1.0f/60.0f);

		// Start tweening?
		if( timeUntilTweenStart <= 0 ) {
			isTweening = true;
			tweenTime = 0;

			this->chooseNewColors();
		}

	} else {
		tweenTime += timeMult * (1.0f/60.0f);

		// Tween finished?
		if( tweenTime >= COLOR_TWEEN_TIME ) {
			isTweening = false;
			timeUntilTweenStart = randRange(10.0f, 20.0f);

			// Apply the new color palette
			for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
				stops[s].src_r = stops[s].dest_r;
				stops[s].src_g = stops[s].dest_g;
				stops[s].src_b = stops[s].dest_b;
			}
		}
	}

	phase += timeMult * 0.017f;

	for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
		//stops[s].position = stops[s].center;

		stops[s].position = stops[s].center + stops[s].radius * sinf(stops[s].freq * phase);

		// Set the color r,g,b
		if( isTweening ) {
			float ratio = tweenTime * (1.0f/COLOR_TWEEN_TIME);

			stops[s]._r = lerp( stops[s].src_r, stops[s].dest_r, ratio ) * brightness;
			stops[s]._g = lerp( stops[s].src_g, stops[s].dest_g, ratio ) * brightness;
			stops[s]._b = lerp( stops[s].src_b, stops[s].dest_b, ratio ) * brightness;

		} else {
			stops[s]._r = stops[s].src_r * brightness;
			stops[s]._g = stops[s].src_g * brightness;
			stops[s]._b = stops[s].src_b * brightness;
		}
	}
}

RGB8 Palette::rgbAtPosition( float pos ) {
	float posFract = pos - floorf(pos);	// 0..1

	// Find the two surrounding stops
	uint_fast8_t prev = STOP_COUNT-1;
	uint_fast8_t next = 0;
	for( uint_fast8_t s=0; s<STOP_COUNT; s++ ) {
		if( posFract < stops[s].position ) {
			prev = (s==0) ? (STOP_COUNT-1) : (s-1);
			next = s;
			break;
		}
	}

	// Lerp between the 2 RGB's

	// Special weird thing when getting colors from the wraparound space
	// between last and first stop.
	if( next == 0 ) {
		stops[next].position += 1.0f;

		// Ensure we're sampling within the prev...next range
		if( posFract < stops[prev].position ) {
			posFract += 1.0f;
		}
	}

/*
	if( (next >= STOP_COUNT-1) || (prev >= STOP_COUNT-1) ) {
		return (RGB8){0, 0, 0x44};
	}
	*/

	float distance = stops[next].position - stops[prev].position;
	float ratio = (posFract - stops[prev].position) / distance;

	// Undo the special wraparound
	if( next == 0 ) {
		stops[next].position -= 1.0f;
	}

	return (RGB8){
		(uint_fast8_t)(lerp( stops[prev]._r, stops[next]._r, ratio )),
		(uint_fast8_t)(lerp( stops[prev]._g, stops[next]._g, ratio )),
		(uint_fast8_t)(lerp( stops[prev]._b, stops[next]._b, ratio ))
	};
}

#endif
