#include <OctoWS2811.h>
#include "Const.h"
#include "Color.h"
#include "FrameParams.h"
#include "Palette.h"

#include "Flow_Base.h"
#include "FlowCentrifuge.h"
#include "FlowHorizStripes.h"
#include "FlowMatrixRain.h"
#include "FlowRadial.h"
#include "FlowSaddle.h"
#include "FlowSpiral.h"
#include "FlowWarpStars.h"
//#include "FlowWhiteNoise.h"

DMAMEM int displayMemory[LEDS_PER_STRIP*6];
int drawingMemory[LEDS_PER_STRIP*6];

const int config = WS2811_GRB | WS2811_800kHz;

OctoWS2811 leds(LEDS_PER_STRIP, displayMemory, drawingMemory, config);

const uint_fast8_t BLINK_PIN = 13;
uint_fast8_t blink = 0;

FlowCentrifuge * _flowCentrifuge           = new FlowCentrifuge();
FlowHorizStripes * _flowHorizStripes       = new FlowHorizStripes();
FlowMatrixRain * _flowMatrixRain           = new FlowMatrixRain();
FlowRadial * _flowRadial                   = new FlowRadial();
FlowSaddle * _flowSaddle                   = new FlowSaddle();
FlowSpiral * _flowSpiral                   = new FlowSpiral();
FlowWarpStars * _flowWarpStars             = new FlowWarpStars();
//FlowWhiteNoise * _flowWhiteNoise           = new FlowWhiteNoise();

FrameParams frameParams;

// Flow transitions
Flow_Base **flows; // Array of pointers to Flow_Base's. Initialized in setup()
Flow_Base * activeFlow;
Flow_Base * nextFlow;
int_fast8_t flowCount;

const bool DEV_ALLOW_FLOW_TRANSITIONS = true;
const float MIN_TIME_UNTIL_TRANSITION = 12.0f;
const float MAX_TIME_UNTIL_TRANSITION = 18.0f;

bool isFlowTransition = false;
float timeUntilFlowTransition = 5.0f;
float flowTransitionTime = 0.0f;
float maxFlowTransitionTime = 5.0f;
float prevMap[LED_COUNT];
float nextMap[LED_COUNT];

long previousMillis = 0;
float phase = 0;

float activeSpeed = 1.0f;
float nextSpeed = 1.0f;

Palette * palette = new Palette();

const float DEV_BRIGHTNESS = 1.0f;//0.667f;

void setup() {
  pinMode(BLINK_PIN, OUTPUT);

  // Start dark
  leds.begin();
  leds.show();

  palette->init();

  // Populate anims in the order you want them to display.
  Flow_Base* FLOW_TEMP[] = {
    _flowCentrifuge,
    _flowHorizStripes,
    _flowMatrixRain,
    _flowRadial,
    _flowSaddle,
    _flowSpiral,
    //_flowWhiteNoise,
    _flowWarpStars
  };
  flowCount = sizeof( FLOW_TEMP ) / sizeof( Flow_Base* );

  // Retain ANIMS_TEMP objects permanently
  flows = (Flow_Base**)malloc( flowCount * sizeof(Flow_Base*) );
  for( int_fast8_t i=0; i<flowCount; i++ ) {
    flows[i] = FLOW_TEMP[i];
  }

  activeFlow = flows[0];
  // dev
  activeFlow = _flowSpiral;

  activeSpeed = activeFlow->speedMult();
  activeFlow->reset( prevMap );
}

void loop() {
  // Frame multiplier
  long newMillis = millis();
  uint_fast8_t elapsed = newMillis - previousMillis;
  previousMillis = newMillis;
  frameParams.timeMult = elapsed * (60.0f / 1000);  // 1.0==exactly 60fps.

  frameParams.brightness = DEV_BRIGHTNESS;

  // Cache tween ratio value before drawing scene
  float flowTransitionRatio = 0.0f;
  if( isFlowTransition ) {
    flowTransitionRatio = flowTransitionTime / maxFlowTransitionTime; // 0..1

    // Apply easing: Sine.easeInOut
    flowTransitionRatio = (cosf(flowTransitionRatio * M_PI) - 1.0f) * -0.5f;  // 0..1
  }

  if( isFlowTransition ) {
    phase -= 0.0017f * frameParams.timeMult * lerp(activeSpeed, nextSpeed, flowTransitionRatio);
  } else {
    phase -= 0.0017f * frameParams.timeMult * activeSpeed;
  }

  if( phase < 0.0f ) {
    phase += 1.0f;
  }

  palette->perFrame( frameParams );

  // Flow transition?
  if( !isFlowTransition ) {
    timeUntilFlowTransition -= frameParams.timeMult * (1.0f/60.0f);

    // Start transition?
    if( timeUntilFlowTransition <= 0 ) {
      // Only start when we randomly choose a new transition
      nextFlow = flows[randInt(flowCount)];
      if( (nextFlow != activeFlow) && DEV_ALLOW_FLOW_TRANSITIONS ) {
        isFlowTransition = true;
        flowTransitionTime = 0;
        nextSpeed = nextFlow->speedMult();

        nextFlow->reset(nextMap);

        maxFlowTransitionTime = min( activeFlow->bestTransitionTime(), nextFlow->bestTransitionTime() );
      }

    }

  } else {
    flowTransitionTime += frameParams.timeMult * (1.0f/60.0f);

    if( flowTransitionTime >= maxFlowTransitionTime ) {
      isFlowTransition = false;
      timeUntilFlowTransition = randRange(MIN_TIME_UNTIL_TRANSITION, MAX_TIME_UNTIL_TRANSITION);

      // We're using nextFlow now
      activeFlow = nextFlow;
      activeSpeed = nextSpeed;

      // Lazy: just copy the flow values from nextMap to prevMap
      for( uint_fast16_t m=0; m<LED_COUNT; m++ ) {
        prevMap[m] = nextMap[m];
      }
    }
  }

  activeFlow->perFrame( frameParams, prevMap );
  if( isFlowTransition ) {
    nextFlow->perFrame( frameParams, nextMap );
  }

  for( uint_fast8_t x=0; x<GRID_WIDTH; x++ ) {
    for( uint_fast8_t y=0; y<GRID_HEIGHT; y++ ) {
      uint_fast16_t mapPos = MAPPOS(x,y);

      float mapVal;
      if( isFlowTransition ) {
        mapVal = lerp( prevMap[mapPos], nextMap[mapPos], flowTransitionRatio );
      } else {
        mapVal = prevMap[mapPos];
      }

      float intPart;
      float fractPart = modff( mapVal + phase, &intPart );

      RGB8 rgb8 = palette->rgbAtPosition( fractPart );

      leds.setPixel( LEDPOS(x,y),
        // The LEDs I'm using have a lot of flicker. :( Dark colors look pretty bad.
        // So I'm going to gamma-correct the color stops in the Palette, and not
        // on the way out.
        /*
        pgm_read_byte(&LED_GAMMA[rgb8.r]),
        pgm_read_byte(&LED_GAMMA[rgb8.g]),
        pgm_read_byte(&LED_GAMMA[rgb8.b])
        */

        rgb8.r,
        rgb8.g,
        rgb8.b
      );
    }
  }

  leds.show();

  // Blink every 64 updates, to prove we're alive.
  // If the device blinks faster than once per second, our frame rate is good!
  blink++;
  digitalWrite(BLINK_PIN, ((blink&0b111111)==0) ? HIGH : LOW);
}
